const scanner = require('sonarqube-scanner')

scanner(
    {
        serverUrl: 'https://sonar.simplustec.com.br',
        token: 'cd9652df974b12aa81edef8cefcd9cbb5cac0267',
        options: {
            'sonar.projectName': 'Resource Data Event Logger',
            'sonar.projectDescription': 'Create logs for Simplus Resources',
            'sonar.sources': 'src',
            'sonar.tests': 'src/tests',
            'sonar.exclusions': 'src/**/*.spec.ts',
            'sonar.test.inclusions': 'src/**/*.spec.ts',
            'sonar.coverage.exclusions':
                'src/**/*.spec.ts,src/**/*.mock.ts,node_modules/*,coverage/lcov-report/*',
        },
    },
    () => process.exit()
)
